import grpc
from concurrent import futures

from circulo_pb2 import (
    ReturnsCirculo                          # Clase que contiene como atributos de instancia, 
                                            # los datos que contiene la respuesta del servicio
)
from circulo_pb2_grpc import (
    OperacionesCirculoServicer,              # servira para definir lo que hara nuestro 
                                             # servicio
    add_OperacionesCirculoServicer_to_server # servira para asociar el servidor gRPC
                                             # configurado con su funcion respectiva
)



class ServidorChido(OperacionesCirculoServicer):
    '''
    Indicando las acciones del servidor.
    '''

    def CalcularPerimetro(self,request,context):
        '''
        Parametros:
            request: Contendra los datos que envie el cliente
            context: Permitira establecer el codigo de estado para la respuesta
        '''

        print("*"*70)
        print(f"Solicitud con los siguientes parametros:\n{request}")

        radio=request.radio
        perimetro=3.1416*2*radio
        respuesta=ReturnsCirculo(resultado=perimetro)
        return respuesta

    def CalcularArea(self,request,context):
        '''
        Parametros:
            request: Contendra los datos que envie el cliente
            context: Permitira establecer el codigo de estado para la respuesta
        '''

        print("*"*70)
        print(f"Solicitud con los siguientes parametros:\n{request}")

        radio=request.radio
        perimetro=3.1416*radio*radio
        respuesta=ReturnsCirculo(resultado=perimetro)
        return respuesta


def serve():
    
    # Creando un servido gRPC y diciendole que use 10 subprocesos para
    # atender solicitudes(podra atender 10 solicitudes al mismo tiempo)
    servidor_grpc = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    # Diciendole al servidor que configuramos anteriormente( 'servidor_grpc' ), 
    # - ¿que es lo que recibira?
    # - ¿que es lo que hara con lo que reciba?
    # - ¿como debera retornar la respuesta?
    # Las anteriores preguntas se responden asociando el servidor que se configuro
    # con la clase que se definio anteriormente
    add_OperacionesCirculoServicer_to_server(
        ServidorChido(),servidor_grpc
    )
    
    # Indicandole al servidor en que direccion ip y puerto se ejecutara
    servidor_grpc.add_insecure_port("localhost:50051")
    
    # Iniciando el servicio infinitamente
    servidor_grpc.start()

    # Indicando que el servicio terminara de ejecutar cuando lo decidamos
    # y eso puede suceder cuando precionemos Ctrl + C
    servidor_grpc.wait_for_termination()

if __name__ == "__main__":  
    serve()