from circulo_pb2 import (
    ParametrosCirculo           # Clase que contiene como atributos de instancia, 
                                # los datos que contiene los parametros del servicio
)
from circulo_pb2_grpc import (
    OperacionesCirculoStub      # Genera instancias de cliente que nos ayudaran 
                                # las cuales sirven para conectarse al servidor
)

import grpc

# Indicandole al cliente en que direccion ip y puerto se encuentra 
# ejecutandose el servidor
channel=grpc.insecure_channel("localhost:50051")

# Creando una instancia de cliente para comunicarnos con el servidor
cliente=OperacionesCirculoStub(channel)


# Creando el objeto con los valores de los parametros que 
# requiere el servicio
parametros_cliente=ParametrosCirculo(radio=10)

# Llamando al servidor a su funcion CalcularArea
respuesta_area=cliente.CalcularArea(parametros_cliente)

# Llamando al servidor a su funcion CalcularPerimetro
respuesta_perimetro=cliente.CalcularPerimetro(parametros_cliente)


# Imprimiendo la respuesta
print("Area del circulo:",respuesta_area)

# Imprimiendo la respuesta
print("Perimetro del circulo:",respuesta_perimetro)


