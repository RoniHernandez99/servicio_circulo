# Nombre del servidor: **OperacionesCirculo**


## **1) Recomendaciones**

Se recomienda instalar los paquetes en un entorno virtual la cual es una herramienta para crear entornos Python aislados, con el fin de evitar problemas de dependencias y versiones, si desea consultar información de como instalar un entorno virtual recomiendo el siguiente enlace: https://docs.python.org/es/3/tutorial/venv.html

### 1.1) Crear entorno virtual

Windows

```
python -m venv env
```

Mac/Linux

```
python3 -m venv env
```

### 1.2) Activar entorno virtual

Windows

```
env\Scripts\activate.bat
```

Mac/Linux

```
source env/bin/activate
```

## **2) Instalar los paquetes necesarios**

```
pip install -r requirements.txt
```

## **3) Contexto del proyecto**

El proyecto consiste en un servicio implementado por medio de GRCP y un cliente que consume dicho servicio.


<div class="myWrapper"  align="center">
<img  src="recursos_readme/imagen_1.png" style="width:50%;"  />
</div>



### 3.1) Servidor


¿Que funciones ejecutara el servidor?

* CalcularArea
* CalcularPerimetro

¿Cuales son los datos que espera recibir el servicio para poder funcionar?

* radio

¿Cuales son los datos que retornara el servicio?

* resultado del area del circulo
* resultado del perimetro del circulo

¿En que lenguage se implementara el servidor?

* En python


### 3.2) Cliente

El cliente hara peticiones al servidor para obtener el area y perimetro del circulo

¿En que lenguage se implementara el cliente?

* En python

## **4)Herramientas utiles**

Con el siguiente comando se generaran en codigo python los dos archivos que son representados del archivo con extension ".proto"

```
python -m grpc_tools.protoc -I . --python_out=.  --grpc_python_out=. circulo.proto
```

<div class="myWrapper"  align="center">
<img  src="recursos_readme/imagen_2.png" style="width:100%;"  />
</div>



